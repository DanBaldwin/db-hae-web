// doc ready shorthand
$(function() {

	// counters for message form validation and 
	// user feedback
	let messageLength = 0;
	let authorLength = 0;

	// 
	// Utilities
	// 
	// clear any modals on close
	$('.modal').on('hidden.bs.modal', function (e) {
	 	$(this).find("input,textarea,select")
	       .val('')
	       .end();

		messageLength = 0;
		authorLength = 0;

		// make sure the update button is hidden 
		// and the new utton is showing

		$('#post-message').show();
 		$('#update-message').hide();

 		// reset the char count
		$('#message-char-count').text(messageLength);
	})

	// hacky way to try and get the right url to post to
	// even if you have a different set up to my dev
	// still requires http://some-host/some-dir
	function getBaseUrl(){
		let location = window.location;
		let baseUrl = location.protocol + '//' + location.host + '/' + location.pathname.split('/')[1];
		return baseUrl;
	}

	// 
	// Messages
	// 

	

	// simple validation
	// don't let empty forms be submitted
	$('#message-body-in, #message-author-in').on('input', function(){
		authorLength = $('#message-author-in').val().length;
		messageLength = $('#message-body-in').val().length;

		if (messageLength && authorLength) {
			$('#post-message')
				.prop('disabled', false)
				.removeClass('btn-primary')
				.addClass('btn-success');
		}else{
			$('#post-message')
				.prop('disabled', true)
				.removeClass('btn-success')
				.addClass('btn-primary');
		}
	});

	// count length of input in message body
	// and feed back to user.
	$('#message-body-in').on('input', function(){
		$('#message-char-count').text(messageLength);
	});

	// post a new message
	$('#post-message').on('click', function(){
		
		let postUrl = getBaseUrl() + '/message/new';

		let author = $('#message-author-in').val();
		let body = $('#message-body-in').val();

		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: postUrl,
			data: { 'author': author, 'body': body },
			success: function(resp){
			 	if (resp['status'] == 'success') {
			 		// reload the index page to show new message
			 		window.location.reload();
			 	}else{
			 		// display an alert to the user
			 		$("#new-message-alert")
				 		.removeClass('alert-success')
				 		.addClass('alert-warning')
				 		.html('<strong>Something Went Wrong: </strong>' + resp['message']).fadeTo(2000, 500)
				 		.slideUp(500, function(){
						    $("#new-message-alert").slideUp(500);
						});
			 	}
			 },
			 error: function(resp){
			 	console.log('something went wrong - response: \n');
			 	console.log(resp);
			 }
		});
	});

	// 
	// Admin Stuff
	// 

	// admin login 
	$('#admin-login').on('click', function(){
		
		let postUrl = getBaseUrl() + '/admin-user/authorise';

		let username = $('#admin-username-in').val();
		let password = $('#admin-password-in').val();

		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: postUrl,
			data: { 'userName': username, 'password': password },
			success: function(resp){
			 	if (resp['status'] == 'success') {
			 		// reload to show admin controls
			 		window.location.reload();
			 	}else{
			 		// display an alert to the user
			 		$("#admin-login-alert")
				 		.removeClass('alert-success')
				 		.addClass('alert-warning')
				 		.html('<strong>Something Went Wrong: </strong>' + resp['message']).fadeTo(2000, 500)
				 		.slideUp(500, function(){
						    $("#new-message-alert").slideUp(500);
						});
			 	}
			 },
			 error: function(resp){
			 	console.log('something went wrong - response: \n');
			 	console.log(resp);
			 }
		});
	});

	//admin log out
	$('#admin-logout').on('click', function(e){
		e.preventDefault();

		let postUrl = getBaseUrl() + '/admin-user/logout';

		$.ajax({
			method: 'GET',
			dataType: 'json',
			url: postUrl,
			success: function(resp){
			 	if (resp['status'] == 'success') {
			 		// reload to hide admin controls
			 		window.location.reload();
			 	}else{
			 		// display an alert to the user
			 		$("#body-alert")
				 		.removeClass('alert-success')
				 		.addClass('alert-warning')
				 		.html('<strong>Something Went Wrong: </strong>' + resp['message']).fadeTo(2000, 500)
				 		.slideUp(500, function(){
						    $("#new-message-alert").slideUp(500);
						});
			 	}
			 },
			 error: function(resp){
			 	console.log('something went wrong - response: \n');
			 	console.log(resp);
			 }
		});

	});

	// delete message
	$('.admin-control.delete').on('click', function(){

		let postUrl = getBaseUrl() + '/message/delete/' + $(this).data('delete-message');

		$.ajax({
			method: 'DELETE',
			dataType: 'json',
			url: postUrl,
			success: function(resp){
			 	if (resp['status'] == 'success') {
			 		// reload to hide admin controls
			 		window.location.reload();
			 	}else{
			 		// display an alert to the user
			 		$("#body-alert")
				 		.removeClass('alert-success')
				 		.addClass('alert-warning')
				 		.html('<strong>Something Went Wrong: </strong>' + resp['message']).fadeTo(2000, 500)
				 		.slideUp(500, function(){
						    $("#new-message-alert").slideUp(500);
						});
			 	}
			 },
			 error: function(resp){
			 	console.log('something went wrong - response: \n');
			 	console.log(resp);
			 }
		});
	});

	// edit message

	//show message in a modal:
	$('.admin-control.edit').on('click', function(){

		let messageId = $(this).data('edit-message');

		let postUrl = getBaseUrl() + '/message/show/' + messageId;

		$.ajax({
			method: 'GET',
			dataType: 'json',
			url: postUrl,
			success: function(resp){
			 	if (resp['status'] == 'success') {
			 		
			 		// set the message modal up for editing instead
			 		$('#message-author-in').val( resp['author'] );
			 		$('#message-body-in').val( resp['messageBody'] );
			 		$('#message-char-count').text( resp['messageBody'].length );

			 		$('#post-message').hide();
			 		$('#update-message').show();
			 		$('#update-message').data('edit-message', messageId);

			 		$('#new-message-modal').modal('toggle');
			 	}else{
			 		// display an alert to the user
			 		$("#body-alert")
				 		.removeClass('alert-success')
				 		.addClass('alert-warning')
				 		.html('<strong>Something Went Wrong: </strong>' + resp['message']).fadeTo(2000, 500)
				 		.slideUp(500, function(){
						    $("#new-message-alert").slideUp(500);
						});
			 	}
			 },
			 error: function(resp){
			 	console.log('something went wrong - response: \n');
			 	console.log(resp);
			 }
		});
	});


	
	//show message in a modal:
	$('#update-message').on('click', function(){

		let messageId = $(this).data('edit-message');
		let author = $('#message-author-in').val();
		let body = $('#message-body-in').val();


		console.log(messageId);

		let postUrl = getBaseUrl() + '/message/update/' + messageId;

		$.ajax({
			method: 'POST',
			dataType: 'json',
			data: { 'author': author, 'body': body },
			url: postUrl,
			success: function(resp){
			 	if (resp['status'] == 'success') {			 		
			 		window.location.reload();
			 	}else{
			 		// display an alert to the user
			 		$("#new-message-alert")
				 		.removeClass('alert-success')
				 		.addClass('alert-warning')
				 		.html('<strong>Something Went Wrong: </strong>' + resp['message']).fadeTo(2000, 500)
				 		.slideUp(500, function(){
						    $("#new-message-alert").slideUp(500);
						});
			 	}
			 },
			 error: function(resp){
			 	console.log('something went wrong - response: \n');
			 	console.log(resp);
			 }
		});
	});

	

});