<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

// auto load the application files
require_once 'Autoloader.php';
$loader = new Autoloader();
$loader->register();

// create and run an application
use Framework\Core\Application;
$app = new Application();
$app->run();
