CREATE DATABASE db_hae_web;

-- I'd make it more secure in real life obviously
CREATE USER 'db_hae_web_user'@'localhost' IDENTIFIED BY 'db_hae_web_pass';

GRANT ALL PRIVILEGES ON db_hae_web.* TO 'db_hae_web_user'@'localhost';

CREATE TABLE `db_hae_web`.`test` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`message` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`)
);

-- my dev MySQL is pretty old (5.5) so I'll have to manage updated at manually.
CREATE TABLE `db_hae_web`.`messages` (
	`id` INT unsigned NOT NULL AUTO_INCREMENT,
	`body` VARCHAR(255) NOT NULL CHARACTER SET utf8 COLLATE utf8_unicode_ci,
	`author` VARCHAR(100) NOT NULL CHARACTER SET utf8 COLLATE utf8_unicode_ci,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT 0,
	`updated_by` INT unsigned NULL,
	PRIMARY KEY (`id`)
);

-- Some default messages
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', 'Maddox Hambleton');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.', 'Peggy Royceston');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the gre', 'Kip Street');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pro', 'Lennard Fowler');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar,.', 'Algar Leighton');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large.', 'Kipling Thorburn');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was.', 'Madeline Appleby');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could se', 'Malinda Knight');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymph', 'Danika Seaver');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Spicy jalapeno bacon ipsum dolor amet rump cow sirloin flank pork. Porchetta drumstick capicola fatback pastrami ball tip shoulder ribeye kevin corned beef tail jowl kielbasa cow brisket. Flank buffal', 'Edmund Watt');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio urna, tempus molestie, porttitor ut, iaculis quis, sem. Phasellus rhoncus. Aenean id metus id velit', 'Patience Granger');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Claymore mine tube disposable car courier numinous saturation point. Wonton soup San Francisco assault market fluidity geodesic 8-bit. Motion stimulate claymore mine soul-delay market jeans tattoo DIY ', 'Fae Martin');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Kowloon refrigerator range-rover sentient Tokyo hacker tube Legba weathered military-grade A.I. math. Sub-orbital drone wristwatch construct girl tower tube A.I. boat artisanal tiger-team bomb chrome ', 'Jan Arkwright');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('boy San Francisco range-rover geodesic. Concrete post-youtube knife physical girl bicycle San Francisco man. Claymore mine hacker courier vehicle military-grade convenience store market wonton soup ', 'Lexia Southgate');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('meta-semiotics refrigerator. Wonton soup advert concrete kanji computer Tokyo vehicle hacker gang jeans refrigerator convenience store Kowloon physical marketing wristwatch. Nodality city cardboard ', 'Kaley Blake ');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('BASE jump bicycle shoes camera pen free-market office systemic systema footage assault katana. Motion grenade uplink car receding long-chain hydrocarbons modem free-market post-human faded knife San ', 'Addilyn Andrews');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Francisco soul-delay tattoo. Urban San Francisco wristwatch military-grade shoes nano-drugs. Meta-boy neural carbon otaku range-rover realism Kowloon bridge tattoo corrupted sign disposable into sensory. ', 'Beryl Rider');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('Corrupted industrial grade tube ablative geodesic wristwatch dissident wonton soup. Dome boat sensory narrative RAF crypto-modem math-skyscraper bridge-space gang A.I. computer. Cyber-Legba digital franchise ', 'Gerry Stern');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('vinyl woman faded denim dolphin drugs. Weathered rifle chrome carbon neon futurity augmented reality. Tanto A.I. tattoo modem convenience store San Francisco paranoid meta-range-rover neural boy knife. Woman ', 'Marybeth Tailor');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('urban fluidity math-dome office marketing garage chrome pre-sprawl cartel DIY shoes claymore mine vinyl franchise. Pen numinous fetishism saturation point concrete smart-pistol dead. Monofilament 8-bit tube lights ', 'Renie Glover');
INSERT INTO `db_hae_web`.`messages` (`body`, `author`) VALUES ('fetishism city systemic papier-mache Shibuya. Construct cyber-alcohol RAF systemic j-pop assault industrial grade lights dome franchise. Motion cyber-voodoo god crypto-numinous tanto Kowloon rebar table monofilament', 'Mercy Tyrell');

-- admin users
CREATE TABLE `db_hae_web`.`admin_users` (
	`id` INT unsigned NOT NULL AUTO_INCREMENT,
	`user_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci UNIQUE NOT NULL,
	`password` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT 0,
	`updated_by` INT unsigned NULL,
	PRIMARY KEY (`id`)
);

-- add a default admin user
-- username: admin
-- password: Password
INSERT INTO `db_hae_web`.`admin_users` (`user_name`, `password`) VALUES ('admin', '$2y$10$MwGVp/e/259G484tiNTEQOCBRzJx9jmPDLFuQ1BQOOP7N08Vb8gAO');






