<?php 

/**
 * Simple autooader class.
 * Define any directories that need files including.
 * Allows use of namespacing etc.
 */
class Autoloader
{

	// define directories to autooad
	const AUTOLOAD_CLASS_DIRS = [
		'App',
		'Framework',
	];


	/**
	 * public interface to trigger autoload
	 * 
	 * @param  bool $debug output search/include process
	 * 
	 * @return void
	 */
	public function register(bool $debug=false)
	{
		foreach (self::AUTOLOAD_CLASS_DIRS as $path) {
			$this->includeRecursive($path, $debug);
		}
	}


	/**
	 * @param  the current path to traverse
	 * @param  bool|boolean whether to output the files being included (as html comments)
	 * @return [type]
	 */
	private function includeRecursive( string $path, bool $debug=false)
	{
		if( $debug ) {echo "<!-- searching {$path} -->\n";}	
		
		$paths = [];

		foreach( glob( "{$path}/*" ) as $filename){
			// php file, include it
			if( strpos( $filename, '.php') !== FALSE){ 
				include_once $filename;
				if( $debug ) echo "<!-- included: {$filename} -->\n";
		    } else {
		    	// directory, stash it for later traversal
		    	$paths []= $filename; 
		    }
		}

		if( $debug ){
			echo "<!-- paths left to search:";
			print_r($paths);
			echo " -->\n";
		}
		# process any dirs that were found.
		foreach ( $paths as $path ) {
			$this->includeRecursive($path, $debug);
		}
	}
}