<?php

namespace App\Models;

use Framework\Core\Database;
// use \PDO;


/**
 * Simple message model.
 *
 * I'd really rather have seperate model and data access layers.
 *
 * I'd also like to have Models as static classess to avoid having to instatiate in 
 * controllers just to call a function, but as they maintain a db connection
 * they technically are stateful.
 *
 * One way to fix would be by some DI container.
 * 
 *
 * but what with time constraints, I think this will do.
 */
class Message
{
	private $db;
	
	function __construct()
	{
		$this->db = Database::getConnection();
	}

	public function total()
	{
		$sql = "SELECT COUNT(*) AS total FROM messages";

		$query = $this->db->prepare( $sql );
		$query->execute();

		return $query->fetchColumn();
	}


	public function get( $page )
	{

		$offset = ($page - 1) * 6;

		$sql = "SELECT * FROM messages ORDER BY created_at DESC LIMIT 6 OFFSET :offset";

		$query = $this->db->prepare( $sql );
		$query->execute( [':offset' => $offset] );

		$response = [
			'currentPage' => $page,
			'total' => $this->total(),
			'messages' => $query->fetchAll()
		];

		return $response;
		
	}

	public function fetch( $id )
	{

		$sql = "SELECT * FROM messages WHERE id = :id";

		$query = $this->db->prepare( $sql );
		$query->execute( [':id' => $id] );

		return $query->fetch();
		
	}

	public function create( $author, $body )
	{

		$sql = "INSERT INTO messages (author, body) VALUES (:author, :body)";

		$query = $this->db->prepare( $sql );

		// return true or false
		return $query->execute([
			':author' => $author,
			':body' => $body
		]);
		
	}

	public function update( $id, $author, $body )
	{

		$sql = "UPDATE messages SET author = :author, body = :body WHERE id = :id ";

		$query = $this->db->prepare( $sql );

		// return true or false
		return $query->execute([
			':id' => $id,
			':author' => $author,
			':body' => $body
		]);
		
	}

	public function destroy( $id )
	{

		$sql = "DELETE FROM messages WHERE id = :id";

		$query = $this->db->prepare( $sql );

		// return true or false
		return $query->execute([':id' => $id]);
		
	}
	


}