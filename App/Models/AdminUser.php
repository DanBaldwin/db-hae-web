<?php

namespace App\Models;

use Framework\Core\Database;
// use \PDO;


/**
 * Simple message model.
 *
 * I'd really rather have seperate model and data access layers.
 *
 * I'd also like to have Models as static classess to avoid having to instatiate in 
 * controllers just to call a function, but as they maintain a db connection
 * they technically are stateful.
 *
 * One way to fix would be by some DI container.
 * 
 *
 * but what with time constraints, I think this will do.
 */
class AdminUser
{
	private $db;
	
	function __construct()
	{
		$this->db = Database::getConnection();
	}

	public function authorise($userName, $password)
	{
		
		$sql = "SELECT id, user_name, password FROM admin_users WHERE user_name = :user_name";

		$query = $this->db->prepare( $sql );
		$query->execute([':user_name' => $userName]);

		$result = $query->fetch();

		if ( !$result ) return false;

		$passVerified = password_verify($password, $result['password']);

		return $passVerified ? $result : false;
	}

}