<?php

namespace App\Controllers;

use App\Controllers\Message;
use App\Models\AdminUser as Model;
use App\Views\Renderer;

use Framework\Helpers\Response;

/**
 * authorise admin users
 */
class AdminUser
{

	/**
	 * authorise admin user password and start a session
	 */
	public static function authorise()
	{
		
		$response = [
			'status' => 'error',
			'message' => ''
		];

		$userName = $_POST['userName'] ?? null;
		$password = $_POST['password'] ?? null;

		if (!$userName || !$password ) {
			$response['message'] = "Username or Password Incorrect";
			Response::toAjax($response);

		}

		// admin user id or false
		$auth = (new Model())->authorise($userName, $password);

		if ( $auth !== false ){
			$response['status'] = 'success';
			$response['userName'] = $auth['user_name'];

			session_start();
			$_SESSION['admin'] = [
				'user' => $auth['user_name'],
				'id' => $auth['id']
			];
		}else{
			$response['message'] = "Username or Password Incorrect";
		}
		
		Response::toAjax($response);
	}

	/**
	 * destroy session
	 */
	public static function logout()
	{
		
		session_start();
		$response['status'] = session_destroy() ? 'success' : 'error';

		Response::toAjax($response);
	}

}