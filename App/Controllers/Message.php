<?php

namespace App\Controllers;

use App\Models\Message as Model;
use App\Views\Renderer;

use Framework\Helpers\Response;
use Framework\Helpers\Auth;

/**
 * args passed in to functions are
 * the url parameters after web/root/controller/action
 */
class Message
{

	public static function index($args=[]) : void
	{
		$page = 1;
		$messages = (new Model())->get($page);
		Renderer::Message('ViewIndex', $messages);
	}

	/**
	 * list the selected page of messages.
	 *
	 * I know I could use index for this but
	 * I have to show my router in action somehow.
	 * 
	 * @param  array $args [pageNumber]
	 * @return void
	 */
	public static function list($args=[]) : void
	{
		// if no page explicitly set, set it to 1
		$page = $args[0] ?? 1;
		$messages = (new Model())->get($page);
		Renderer::Message('ViewIndex', $messages);
	}

	/**
	 * destroy a message
	 * @param  array $args[0] = message id
	 */
	public static function show($args=[])
	{

		$response = [
			'status' => 'error',
			'message' => ''
		];

		// message id
		if (!isset($args[0]) || !is_numeric($args[0])) {
			$response['message'] = "No message id set";
			Response::toAjax($response);
		}


		$id = $args[0];

		$message = (new Model())->fetch( $id );

		if ( $message ){
			$response['status'] = 'success';
			$response['author'] = $message['author'];
			$response['messageBody'] = $message['body'];

		}else{
			$response['message'] = "Problem Getting Message.";
		}

		Response::toAjax($response);
		
	}

	/**
	 * create a new message
	 */
	public static function new()
	{
		
		$response = [
			'status' => 'error',
			'message' => ''
		];

		$author = $_POST['author'] ?? null;
		$body = $_POST['body'] ?? null;

		if (!$author || !$body ) {
			$response['message'] = "Author Name or Message Body Not Set";
			Response::toAjax($response);

		}

		if ( (new Model())->create( $author, $body ) ){
			$response['status'] = 'success';
		}else{
			$response['message'] = "Problem Saving Message.";
		}
		
		Response::toAjax($response);
	}

	/**
	 * destroy a message
	 * @param  array $args[0] = message id
	 */
	public static function update($args=[])
	{

		$response = [
			'status' => 'error',
			'message' => ''
		];

		// message id
		if (!isset($args[0]) || !is_numeric($args[0])) {
			$response['message'] = "No message id set";
			Response::toAjax($response);
		}

		// verify the user can update messages
		session_start();
		if( !Auth::loggedInAsAdmin() ){
			$response['message'] = "Not authorised";
			Response::toAjax($response);
		}

		$author = $_POST['author'] ?? null;
		$body = $_POST['body'] ?? null;

		// check both author and message have been set
		if (!$author || !$body ) {
			$response['message'] = "Author Name or Message Body Not Set";
			Response::toAjax($response);

		}


		$id = $args[0];

		if ( (new Model())->update( $id, $author, $body ) ){
			$response['status'] = 'success';
		}else{
			$response['message'] = "Problem Updating Message.";
		}

		Response::toAjax($response);
	}
		

	/**
	 * destroy a message
	 * @param  array $args[0] = message id
	 */
	public static function delete($args=[])
	{

		$response = [
			'status' => 'error',
			'message' => ''
		];

		// message id
		if (!isset($args[0]) || !is_numeric($args[0])) {
			$response['message'] = "No message id set";
			Response::toAjax($response);
		}

		// verify the user can delete messages
		session_start();
		if( !Auth::loggedInAsAdmin() ){
			$response['message'] = "Not authorised";
			Response::toAjax($response);
		}

		
		$id = $args[0];

		if ( (new Model())->destroy( $id ) ){
			$response['status'] = 'success';
		}else{
			$response['message'] = "Problem Deleting Message.";
		}

		Response::toAjax($response);
		
	}

	

	

}