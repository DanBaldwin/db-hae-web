<?php
namespace App\Views\Message;

use App\Views\BaseView;

use Framework\Helpers\Auth;
use Framework\Helpers\Path;
use Framework\Helpers\Route;
use Framework\Helpers\Timestamp;


class ViewIndex extends BaseView{

	/**
	 * function to deal with rendering logic for the messages/index page.
	 *
	 * Not really a fan of passing $args arrays but that's what happens when you
	 * decide to implement an MVC framework quickly, using only the PHP standard library =0]
	 * 
	 * @param  $args message Model
	 * [
	 *  [
	 *      'currentPage' => int,
	 *  	'total' => int,
	 *  	'messages' => [Message messages] // limited to 6 offset to page
	 *  ]
	 * ]
	 * @return string output buffered HTML
	 */
	protected static function bodyContent( $args )
	{

		$currentPage = $args[0]['currentPage'];
		$total = $args[0]['total'];
		$messages = $args[0]['messages'];

		ob_start();
		?>
			<div class="row h-100">
				<?= self::getSideBar(); ?>
				<?= self::getMessages( $currentPage, $total, $messages ); ?>
			</div>

			<!-- put modals as close to root container as possible -->
			<?= self::getNewPostModal() ?>
			<?= self::getAdminLoginModal() ?>

		<?php
		return ob_get_clean();
	}


	/**
	 * compose all of the messages
	 */
	private static function getMessages( $currentPage, $total, $messages )
	{
		ob_start();
		?>
			<div id="messages" class="col-lg-9 col-md-12">

				<?php
					$i = 0;
					foreach ( $messages as $message ) {
						// start a row on even indices
						if( $i % 2 == 0 ) echo '<div class="row">';

						echo self::getMessage( $message );
						
						// end a row on odd indices or if last message
						if( $i % 2 == 1 || $i == ( count($messages) - 1 ) ) echo '</div>';

						$i++;
					}
				?>

				<?= self::getPagination( $currentPage, $total ); ?>
			</div>
		<?php
		return ob_get_clean();
	}

	/**
	 * get a single message
	 */
	private static function getMessage( $message )
	{
		ob_start();
		?>
			<div class="col-md-6 message">
						
						<div class="message-content">

							<div class="message-body-wrap">
								<div class="message-body">

									<p>
										<?=  $message['body'] ?>
									</p>								
								</div>
								
								<div class="message-icon">
									<i class="fas fa-quote-left"></i>
								</div>	
							</div>

							<div class="row">
								<div class="author-row col-8">

										<div class="message-author">
											<?= $message['author'] ?>
										</div>
									
										<div class="message-date">
											<?= Timestamp::toPretty( $message['created_at'] ) ?>
										</div>
									
								</div>

								<?= self::getAdminControls($message['id']) ?>
							</div>
						</div>

												
					</div>

		<?php
		return ob_get_clean();
	}

	/**
	 * edit and delete buttons for logged in admin users
	 * @param  int $messageId
	 */
	private static function getAdminControls( $messageId )
	{
		if ( !Auth::loggedInAsAdmin() ) return;

		ob_start();
		?>
			<div class="admin-controls col-3">
				<div class="row">
					<!-- edit -->
					<div class="col-6 admin-col">
						<div class="admin-control edit" data-edit-message="<?= $messageId ?>">
							<i class="fas fa-pencil-alt"></i>
						</div>
					</div>

					<!-- delete -->
					<div class="col-6 admin-col">
						<div class="admin-control delete" data-delete-message="<?= $messageId ?>">
							<i class="fas fa-trash-alt"></i>
						</div>
					</div>
				</div>
			</div>
		<?php
		return ob_get_clean();
	}

	/**
	 * get the pagination links.
	 * @param  int $currentPage
	 * @param  int $total - number of messages
	 */
	private static function getPagination( $currentPage, $total )
	{
		// this logic could be moved to controller or model layers or
		// even a helper but I quite like it here so you can see 
		// whats happening to create the HTML

		$pageCount = ceil( $total / 6 );

		if ( $pageCount < 5 ) {
			$linkRange = range( 1, $pageCount);
		}else{
			// we only want to display 2 pages either side of current
			// we'll give the first and last page on '<' and '>'
			$linkRange = range( 1, 5 );

			// get the 2 either side of current page
			if ( $currentPage  > 2 ) $linkRange = range( $currentPage - 2, $currentPage + 2 );

			// unles we're near the last page.
			// then show last page and the four previous pages
			if ( $pageCount - $currentPage < 2) $linkRange = range( $pageCount - 4, $pageCount );
		}

		ob_start();
		?>
		<div class="row">
			<div class="col">
				<nav>
				    <ul class="pagination">
				    	<!-- link to first page -->
						<li class="page-item"><a class="page-link" href="<?= Route::message( 'list', 1 ) ?>"> < </a></li>
						
						<?php 

							foreach ( $linkRange as $page ) {
								echo self::getPaginationLink( $page, $page == $currentPage );
							}

						?>
						<!-- link to last page -->
						<li class="page-item"><a class="page-link" href="<?= Route::message( 'list', $pageCount ) ?>"> > </a></li>
					</ul>
				</nav>
			</div>
		</div>
			
		<?php
		return ob_get_clean();
	}

	/**
	 * get a single numbered pagination link
	 * @param  int $page which page is the link for
	 * @param  bool $isCurrentPage for different styling
	 */
	private static function getPaginationLink( int $page, bool $isCurrentPage )
	{
		ob_start();
		?>
			<li class="page-item">
				<a class="page-link <?= $isCurrentPage ? 'current-page' : '' ?>" href="<?= Route::message( 'list', $page ) ?>">
					<?= $page ?>
				</a>
			</li>

		<?php
		return ob_get_clean();
	}


	private static function getSideBar()
	{
		ob_start();
		?>
			<div id="sidebar" class="col-lg-3 col-md-12">
				<div class="row">
					<div class="col">
						<div id="sidebar-logo">
							<img src="<?= Path::img('hae-logo.svg'); ?>" alt="">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col">
						<div class="text-center">
							<h3 id="sidebar-gb">Guestbook</h3>
						</div>
					</div>
				</div>

				
				<div class="row">
					<div class="col">
						<div class="text-justified p-4">
							<p>
								Feel free to leave us a short message to tell us what you think of our services.
							</p>
							<!-- <a href="#"> -->
							<div id="post-button" class="text-center" data-toggle="modal" data-target="#new-message-modal">
								<p>
									Post a Message
								</p>
							</div>
							<!-- </a> -->
						</div>
					</div>
				</div>
				
				<div id="admin-link" class="text-center">
					<p>
						<?= self::getAdminLink() ?>
					</p>
				</div>
	
			</div>

		<?php
		return ob_get_clean();
	}

	/**
	 * If admin logged in, give a log out link.
	 */
	private static function getAdminLink()
	{
		if ( Auth::loggedInAsAdmin() ) {
			$user = Auth::adminUserName();
			return "Logged in as: {$user} | <a href='' id='admin-logout'>Log out</a>";
		}else{
			return '<a href="" data-toggle="modal" data-target="#admin-login-modal">Admin Login</a>';
		}
	}

	private static function getNewPostModal()
	{
		ob_start();
		?>
			<!-- New Message Modal -->
			<div class="modal fade" id="new-message-modal" tabindex="-1" role="dialog" aria-labelledby="new-message-modal-title" aria-hidden="true">
				<!-- alert for error feedback to user -->
				<div id="new-message-alert" class="alert text-center" role="alert">
				</div>

				<div class="modal-dialog modal-dialog-centered" role="document">    
					<div class="modal-content">
						<!-- modal header -->
						<div class="modal-header">
							<h5 class="modal-title" id="new-message-modal-title">Post Your Message.</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								 <span aria-hidden="true">&times;</span>
							</button>
						</div>
						
						<!-- modal body -->
						<div class="modal-body">
							<form>
								<div class="form-group">
									<input type="text" class="form-control" id="message-author-in"  placeholder="Your Name" maxlength="100">
								</div>

								<div class="form-group">
									<label for="message-body-in">Your Message</label>
									<textarea class="form-control" id="message-body-in" rows="3" aria-describedby="char-counter" maxlength="250"></textarea>
									<small id="char-counter" class="form-text text-muted"><span id="message-char-count">0</span>/250.</small>
								</div>
							</form>
						</div>
						
						<!-- modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
							<button type="button" id="post-message" class="btn btn-primary" disabled>Save</button>
							<button type="button" id="update-message" class="btn btn-primary" style="display: none;">Save</button>
						</div>

					</div>
				</div>
			</div>
		<?php
		return ob_get_clean();

	}

	private static function getAdminLoginModal()
	{
		ob_start();
		?>
			<!-- New Message Modal -->
			<div class="modal fade" id="admin-login-modal" tabindex="-1" role="dialog" aria-labelledby="admin-login-modal-title" aria-hidden="true">
				<!-- alert for error feedback to user -->
				<div id="admin-login-alert" class="alert text-center" role="alert">
				</div>

				<div class="modal-dialog modal-dialog-centered" role="document">    
					<div class="modal-content">
						<!-- modal header -->
						<div class="modal-header">
							<h5 class="modal-title" id="admin-login-modal-title">Log in.</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								 <span aria-hidden="true">&times;</span>
							</button>
						</div>
						
						<!-- modal body -->
						<div class="modal-body">
							<form id="admin-login-form">
								<div class="form-group">
									<input type="text" class="form-control" id="admin-username-in"  placeholder="Your Name" maxlength="100">
								</div>

								<div class="form-group">
									<input type="password" class="form-control" id="admin-password-in"  placeholder="Password" maxlength="100">
								</div>
							</form>
						</div>
						
						<!-- modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
							<button type="button" id="admin-login" class="btn btn-primary">Log in</button>
						</div>

					</div>
				</div>
			</div>
		<?php
		return ob_get_clean();

	}
}
