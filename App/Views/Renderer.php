<?php

namespace App\Views;

/**
 * Really simple Renderer.
 * 
 * class to cantralise and forward calls to actual view templates.
 */
class Renderer
{
	// base namespace for all views
	const VIEW_NS = "App\\Views\\";
	
	public static function __callStatic($viewLocation, $args)
	{
		// first argument passed to static Renderer calls is the actual page
		$page = array_shift($args);

		// concat all parts to get the full view namespace
		$view = static::VIEW_NS . $viewLocation . "\\{$page}";

		// forward the static call to the views render function with any additional $args
		forward_static_call([$view, 'render'], $args);
	}
}