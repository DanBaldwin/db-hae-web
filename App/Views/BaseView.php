<?php
namespace App\Views;

use Framework\Helpers\Path;

class BaseView{

	public static function render($args)
	{
		session_start();
		echo static::getHeader($args);
		echo static::getBody($args);
		echo static::getFooter($args);
		// page rendered halt execution
		die;

	}


	protected static function getHeader($args)
	{
		ob_start();
		?>
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<title>HAE Group Gusetbook.</title>
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				<!-- google fonts -->
				<link href="https://fonts.googleapis.com/css?family=Caveat:700|IBM+Plex+Sans&display=swap" rel="stylesheet">
				<!-- bootstrap 4.1 -->
				<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
				<!-- fontawesome icons -->
				<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

				<!-- custom styles -->
				<link rel="stylesheet" type="text/css" href="<?= Path::css('style.css'); ?>">
			</head>
		<?php
		return  ob_get_clean();
	}

	protected static function getBody($args)
	{
		ob_start();
		?>
			<body style="min-height: 100vh;">
				<!-- alert for success/error feedback to user -->
				<div id="body-alert" class="alert text-center" role="alert">
				</div>
				<div id="body-wrap">
					<div id="body-content" class="container-fluid" style="height: 100vh;">
						<?= static::bodyContent($args); ?>
					</div>
				</div>
			</body>
		<?php
		return ob_get_clean();
	}

	protected static function bodyContent($args){}

	protected static function getFooter($args)
	{
		ob_start();
		?>
				<!-- JQuery -->
				<script
				  src="https://code.jquery.com/jquery-3.4.1.min.js"
				  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
				  crossorigin="anonymous"></script>

				<!-- bootstrap -->
				<script 
					src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" 
					integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" 
					crossorigin="anonymous"></script>
				
				<script 
					src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" 
					integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" 
					crossorigin="anonymous"></script>

				<!-- custom scripts -->
				<script src="<?= Path::js('scripts.js'); ?>" type="text/javascript" charset="utf-8" async defer></script>

			</html>
		<?php
		return  ob_get_clean();
	}
}



