<?php
namespace App\Views\PageNotFound;

use App\Views\BaseView;

class View404 extends BaseView{

	protected static function bodyContent($args)
	{
		ob_start();
		?>
			<div class="jumbotron jumbotron-fluid">
				<div class="container">
					<h1 class="display-3 fancy-header">404: Page not found.</h1>
					<p class="lead"><?= $args[0] ?>/<?= $args[1] ?> Could not be found on this server.</p>
				</div>
			</div>

			
		<?php
		return ob_get_clean();
	}
}



