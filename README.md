### HAE Group Web Development Test

Firstly thanks for your consideration.

When I first read the brief, I thought, I want to try and do something a bit more adventurous than a simple guest-book page while still sticking to the brief.

Two of my main professional interests are systems architecture and research and development.

As such, I thought I'd try my hand at making an MVC framework.

Everything seems to be working. Obviously it's no Laravel. I'd definitely not be deploying it to my production apps any time soon.

That said, it does the trick and it's been a wild ride learning how to make some things work and think about the limitations presented with using plain old PHP to accomplish the task.

After developing the framework, I got about developing the actual task I'd been set. What was nice was that even though the framework hd just been conceived, it felt very familiar. Again, not quite production level but something in the right ball park.

I hope you like what you see.

### set up

This is the first proper development I've done on any of my machines for a good couple of years (I've had work laptops for a while) and so My environment is pretty out-dated.

I'm running: 

- PHP 7.0.8
- MySQL 5.5

I've had it running on my MacBook Air on MAMP and my Desktop running Kubuntu with a normal LAMP stack set-up.

I've tried to account for you having a different set-up to me but if you struggle to get it running, some of my routing helpers might not be quite there.

I have the project in:
`var/www/html/db-hae-web`

so the server serves them at:
`http://localhost/db-hae-web`

You will need `mod_rewrite` enable for apache2.

The database schema is in `migrations/schema.sql`.

#### Admin User
username: admin
password: Password

If there's anything else, please feel free to let me know.

Many Thanks,

Daniel Baldwin.

