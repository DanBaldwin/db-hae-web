<?php

namespace Framework\Core;

use Framework\Core\Config;

use \PDO;

/**
 * Class to manage database connections.
 *
 * used staically to share a single connection among consumers.
 */
class Database
{
	static $connection;
	public static function getConnection()
	{
		if (!self::$connection) {
			$dbConfig = Config::db();

			$db_host = $dbConfig['host'];
			$db_name = $dbConfig['name'];
			$user = $dbConfig['user'];
			$pw = $dbConfig['password'];

			self::$connection = new PDO( "mysql:host={$db_host};dbname={$db_name}", $user, $pw);
		    self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    self::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}

		return self::$connection;
	}
}