<?php

namespace Framework\Core;

/**
 * Config Values.
 *
 * Split into static functions to seperate values.
 *
 * could be extended to acount for different environments (dev, live, test etc)
 */
class Config
{
	static function db()
	{
		return [
			'host' => 'localhost',
	        'name' => 'db_hae_web',
	        'user' => 'db_hae_web_user',
	        'password' => 'db_hae_web_pass'
		];
	}
}