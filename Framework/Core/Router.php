<?php

namespace Framework\Core;

use App\Views\Renderer;

/**
 * Really simple router.
 * 
 * Not anything special but handcrafted using php standard library.
 * 
 * designed to be a stupid RESTful router.
 *
 * routes are hyphenated-controler-name/action
 *  => HyphenatedControlerName/action
 *
 * Would have been nice to have route types:
 *
 * GET, POST, DELETE etc
 */
class Router
{
	// general namespace for dynamically calling controller classes
	const CONTROLLER_NS = "App\\Controllers\\";
	// the designated home page to be used when no URL params are passed
	const HOME_PAGE = "App\\Controllers\\Message::index";

	public function dispatch( $request )
	{

		// strip the root folder from the request uri
		// just in case you're not uising the same dir structure as me
		$webRoot = str_replace( 'index.php', '', $_SERVER['PHP_SELF'] );
		$endpoint = str_replace( $webRoot, '', $request );

		// remove trailing slash
		$endpoint = rtrim( $endpoint, '/' );

		$args = explode( '/', $endpoint );

		// render home page if empty (root url)
		if ( !$args[0] ) {
			$page = self::HOME_PAGE;
			$page();
		}

		// first get the first item of endpoint
		// split on hyphens
		// make sure all the individual parts are lowercase
		// uppercase the first letter of each part
		// and stick it all back together.
		$controller = implode( '', array_map( 'ucfirst', array_map( 'strtolower', explode( '-', array_shift( $args ) ) ) ) );
		
		$action = strtolower( array_shift( $args ) );

		$controllerClass = self::CONTROLLER_NS . $controller;
		

		if ( !class_exists( $controllerClass ) ) {
			Renderer::PageNotFound( 'View404', $controller, $action );
		}

		if ( !method_exists( $controllerClass, $action ) ) {
			Renderer::PageNotFound( 'View404', $controller, $action );
		}

		$controllerClass::$action($args);
	}
}