<?php

namespace Framework\Core;
/**
 * Class to initialize the application.
 * initializes any required constants and includes any files.
 * autoloads class paths.
 * dispatches/routes requests to the correct controller/action.
 */
class Application
{
	private $router;
	function __construct()
	{

	}

	public function run()
	{
		$this->init();
		
		$this->router->dispatch($_SERVER['REQUEST_URI']);
	}

	private function init() 
	{
		date_default_timezone_set('Europe/London');
		$this->router = new Router();
	}

}