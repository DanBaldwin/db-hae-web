<?php

namespace Framework\Helpers;

/**
 * simple helper class for getting resources/assets from the Public folder
 */
class Auth
{
	/**
	 * function to check if an admin user is logged in
	 */
	public static function loggedInAsAdmin()
	{
		// set in AdminUser controller
		return isset($_SESSION['admin']);
	}

	/**
	 * @return admin username
	 */
	public static function adminUserName() : string
	{
		return $_SESSION['admin']['user'];
	}
}