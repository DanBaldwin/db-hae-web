<?php

namespace Framework\Helpers;

/**
 * simple helper class for getting resources/assets from the Public folder
 */
class Route
{
	/**
	 * my local dev env serves from http://localhost/db-hae-web/...
	 * I've tried to account for changes in your configuration but
	 * I don't currently have any other servers set up.
	 * 
	 * @return string dynamically generated base for the Public directory.
	 */
	public static function base() : string
	{
		// may need changing depending on your server set-up
		// var_dump($_SERVER);
		// die;
		$base = "http://{$_SERVER['SERVER_NAME']}";
		$webRoot = $webRoot = str_replace('/index.php', '', $_SERVER['PHP_SELF']);
		return "{$base}{$webRoot}";
	}

	/**
	 * magic function to generate the route for the given resource, and url params
	 * called like: Route::message('list', 2);
	 * 
	 * @return string the URI of a given RESTful route
	 * (on my machine) http://localhost/db-hae-web/message/list/2
	 */
	public static function __callStatic($resource, $args) : string
	{
		// Route helper base() used by other classes (eg Path Helper)
		if ( strtolower($resource) == 'base') return self::base();

		$urlParams = implode('/', $args);

		return self::base() . "/{$resource}/{$urlParams}";

	}
}