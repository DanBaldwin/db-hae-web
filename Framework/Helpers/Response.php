<?php

namespace Framework\Helpers;

/**
 * simple helper class for getting resources/assets from the Public folder
 */
class Response
{
	/**
	 * Function for returning values to Ajax calls.
	 * Ajax needs the header setting and output (echo)
	 *
	 * header can cause issues if it gets set multiple times (after output).
	 *
	 * so halt execution after outputting response.
	 *  
	 * @param  array $response ['status', 'message', 'anything else']
	 * @return void
	 */
	public static function toAjax( $response )
	{
		header( 'Content-type: application/json' );
		echo json_encode( $response );
		// halt execution to prevent errors from header being set
		die;

	}
}