<?php

namespace Framework\Helpers;

use Framework\Helpers\Route;

/**
 * simple helper class for getting resources/assets from the Public folder
 */
class Path
{
	/**
	 * my local dev env serves from http://localhost/db-hae-web/...
	 * I've tried to account for changes in your configuration but
	 * I don't have different servers etc as I don't currently have 
	 * any nginx or other servers set up. 
	 * @return string dynamically generated base for the Public directory.
	 */
	private static function base() : string
	{
		return Route::base() . "/Public";
	}

	/**
	 * magic function to generate the asset path for the given asset type and asset name.
	 * @return string the URI of a given asset.
	 */
	public static function __callStatic($assetType, $args) : string
	{
		// first argument passed to static Renderer calls is the actual page
		$assetName = array_shift( $args );
		$assetType = strtolower( $assetType );

		return self::base() . "/{$assetType}/{$assetName}";

	}
}