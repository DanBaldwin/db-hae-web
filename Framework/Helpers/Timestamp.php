<?php

namespace Framework\Helpers;

/**
 * simple helper class for getting resources/assets from the Public folder
 */
class Timestamp
{
	/**
	 * function to convert a mysql timestamp value in the form
	 * 21st Mar, 2019 at 09:43am
	 */
	public static function toPretty( $timestamp ) : string
	{
		// sneaky asking for the format to contain 'at'.
		// I had to escape them, still a nice one-liner though.
		return (new \DateTime($timestamp))->format('jS M, Y \a\t h:ia');

	}
}